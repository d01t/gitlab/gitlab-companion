# GitLab Companion: a web extension for CI Tools

GitLab is a wonderful code forge tool, with many possibility, but SAAS version gitlab.com lacks test reports. So I've just made this extension in order to check automatically artifacts if they have a test report and a badge to display.

Badge and report url inside artifacts are hardcoded :
- build/report/index.html
- build/report/badges/report.svg

## Installation

At your convenience:
- Extract latest artifact on "Deploy" job on master branch https://gitlab.com/d01t/gitlab-companion and drag'n drop xpi file on a Firefox window
- If you have enough rights, browse https://addons.mozilla.org/fr/firefox/addon/28ecf9126b8c45d2aa6d/
- If you use Chrome and you have enough rights, browse https://chrome.google.com/webstore/detail/gitlab-companion/hlefnpgcgbmfehngoggingdofcbdchpp

## Development

Prerequisites :
- node
- yarn
- firefox

```bash
git clone https://gitlab.com/d01t/gitlab-companion.git gitlab-companion
cd gitlab-companion
yarn
yarn dev
```

Theses sources are based on :
- https://github.com/fstanis/webextensions-webpack-boilerplate
- https://github.com/Standard8/example-webextension
