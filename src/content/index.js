var stageValidStates = ['success','failed','success-with-warnings'];
var stageValidNames = ['test','report','acceptance','deploy'];
var xhrValidStates = [200,302,304];
var storage = window.localStorage;
var releaseLockTimeout = 110, delayBeforeExecution = 100;
var storagePrefix = 'gitlab-companion';
var l = window.location;
if (
	document.body.getAttribute('data-page') &&
	(-1 !== document.body.getAttribute('data-page').indexOf(':')) &&
	('projects' === document.body.getAttribute('data-page').split(':')[0])
) {
	var ajaxRequest = function(url,type,success,fail) {
		var httpRequest = new XMLHttpRequest();
		httpRequest.onreadystatechange = function() {
			if (httpRequest.readyState === XMLHttpRequest.DONE) {
				if (-1 === xhrValidStates.indexOf(httpRequest.status)) {
					fail(httpRequest);
				}
				else {
					var data = httpRequest.responseText;
					if (-1 !== httpRequest.getResponseHeader('Content-Type').indexOf('application/json')) {
						data = JSON.parse(data);
					}
					success(data, httpRequest);
				}
			}
		};
		httpRequest.open('GET', url, false);
		httpRequest.send();
	};
	
	var fetchReportAndBadgeFromPipeline = function(pipeline) {
		var doBrowseArtifact = false;
		for (var j = 0; j < pipeline.details.stages.length; j++) {
			var stage = pipeline.details.stages[j];
			if (
				(-1 !== stageValidNames.indexOf(stage.name.toLowerCase())) &&
				(-1 !== stageValidStates.indexOf(stage.status.group))
			) {
				doBrowseArtifact = true;
				break;
			}
		}
		var reportUrl = null, badgeUrl = null;
		if (doBrowseArtifact) {
			var artifacts = [];
			for (var j = 0; j < pipeline.details.artifacts.length; j++) {
				var artifact = pipeline.details.artifacts[j];
				var artifactBrowsePath = l.protocol + '//' + l.hostname + artifact.browse_path;
				var artifactPath = artifactBrowsePath.substring(0,artifactBrowsePath.lastIndexOf('/'));
				var artifactFilePath = artifactPath + '/file';
				var artifactRawPath = artifactPath + '/raw';
				var pathHash = storagePrefix + artifact.path;
				if (
					(-1 !== stageValidNames.indexOf(artifact.name.toLowerCase())) &&
					!artifact.expired &&
					'undefined' === typeof storage[pathHash]
				) {
					ajaxRequest(artifactFilePath + '/build/report/badges/report.svg','HEAD',function(data, httpRequest) {
						storage[pathHash] = (200 === httpRequest.status);
					},function(httpRequest) {
						storage[pathHash] = false;
					});
				}
				if (true === storage[pathHash] || 'true' === storage[pathHash]) {
					reportUrl = artifactFilePath + '/build/report/index.html';
					badgeUrl = artifactRawPath +'/build/report/badges/report.svg';
				}
			}
		}
		return reportUrl ? {report: reportUrl, badge: badgeUrl} : null;
	}
	var rewritePageLock = false;
	var releaseRunningLock = function() {
		setTimeout(function() {
			rewritePageLock = false;
		},releaseLockTimeout);
	};
	var rewritePage = function(event) {
		if (rewritePageLock) {
			return;
		}
		rewritePageLock = true;
		l = window.location;
		var endpoint = l.pathname;
		endpoint = l.protocol + '//' + l.hostname + ( endpoint.indexOf('\.json') === -1 ? endpoint + '.json' : endpoint ) + ( l.search.length ? l.search : '' );
		ajaxRequest(endpoint,'GET',function(data, jqXHR) {
			if ('undefined' !== typeof data.pipelines) {
				// Pipelines view
				var pipelineNodes = document.querySelectorAll('.pipelines .commit');
				for (var i = 0; i < pipelineNodes.length; i++) {
					var pipeline = data.pipelines[i];
					var pipelineNode = pipelineNodes[i];
					var reportAndBadge = fetchReportAndBadgeFromPipeline(pipeline);
					var innerHTML = reportAndBadge ? '<p class=""><a href="'+reportAndBadge.report+'" target="_blank"><img src="'+reportAndBadge.badge+'" alt="report" /></a></p>':'';
					var timeAgoColumn = pipelineNode.querySelector('.pipelines-time-ago');
					var actionColumn = pipelineNode.querySelector('.pipeline-actions');
					if ( -1 !== timeAgoColumn.className.indexOf('section-15') ) {
						timeAgoColumn.className = timeAgoColumn.className.replace('section-15','section-10');
						actionColumn.className = actionColumn.className.replace('section-20','section-15');
						var element = document.createElement('div');
						element.className = 'table-section section-10 test-results';
						element.innerHTML = '<div role="rowheader" class="table-mobile-header"></div><div class="table-mobile-content">'+innerHTML+'</div>';
						actionColumn.parentNode.insertBefore(element,actionColumn);
					}
					else if (pipelineNode.querySelector('.test-results .table-mobile-content')) {
						pipelineNode.querySelector('.test-results .table-mobile-content').innerHTML = innerHTML;
					}
				}
			}
			else if ('undefined' !== typeof data.details && 'undefined' !== typeof data.details.stages && 'undefined' !== typeof data.details.artifacts) {
				// Pipeline view
				var reportAndBadge = fetchReportAndBadgeFromPipeline(data);
				if (reportAndBadge) {
					var innerHTML = '<div class="icon-container"><i aria-hidden="true" data-hidden="true" class="fa fa-dashboard"></i></div><a href="'+reportAndBadge.report+'" target="_blank"><img src="'+reportAndBadge.badge+'" alt="report" /></a>';
					if (document.querySelector('.well-segment.test-results')) {
						document.querySelector('.well-segment.test-results').innerHTML = innerHTML;
					}
					else {
						var element = document.createElement('div');
						element.className = 'well-segment test-results';
						element.innerHTML = innerHTML;
						var branchInfo = document.querySelector('.well-segment.branch-info');
						branchInfo.parentNode.append(element);
					}
				}
			}
			else {
				//console.log(data);
			}
			releaseRunningLock();
		},function(httpRequest) {
			// Json endpoint doesn't exist
			var projectId = document.querySelector('.search-autocomplete-opts').getAttribute('data-autocomplete-project-id');
			var commitButton = document.querySelector('.commit-sha-group button');
			var projectBadges = document.querySelector('.project-badges');
			if (projectBadges && commitButton) {
				var commitHash = commitButton.getAttribute('data-clipboard-text');
				var projectEndpoint = l.protocol + '//' + l.hostname + '/api/v4/projects/' + projectId;
				var projectData = null;
				ajaxRequest(projectEndpoint,'GET',function(data, httpRequest) {
					projectData = data;
					ajaxRequest(projectData.web_url+'/pipelines.json?scope=finished&page=1','GET',function(data, httpRequest) {
						if ((('undefined' !== typeof data.pipelines)) && data.pipelines.length) {
							var reportAndBadge = fetchReportAndBadgeFromPipeline(data.pipelines[0]);
							if (reportAndBadge) {
								var innerHTML = '<img src="'+reportAndBadge.badge+'" alt="report" />';
								if (projectBadges.querySelector('.test-results')) {
									projectBadges.querySelector('.test-results').innerHTML = innerHTML;
								}
								else {
									var element = document.createElement('a');
									element.className = 'append-right-8 test-results';
									element.setAttribute('target','_blank');
									element.innerHTML = innerHTML;
									projectBadges.append(element);
								}
								element.setAttribute('href',reportAndBadge.report);
							}
						}
					},function(){});
				},function(){});
			}
			releaseRunningLock();
		});
	};
	
	rewritePage();
	var ajaxObserver = new MutationObserver(function() {
		// Delay to wait GitLab JS modifications
		if (!rewritePageLock) {
			setTimeout(rewritePage,delayBeforeExecution);
		}
	});
	var elementsToObserve = document.querySelectorAll('.pipelines-container, .pipeline-visualization');
	for (var k=0; k < elementsToObserve.length; k++) {
		ajaxObserver.observe(elementsToObserve[k], {
			attributes: true,
			childList: true,
			characterData: true,
			subtree: true,
			attributeOldValue: true,
			characterDataOldValue: true
		});
	}
}