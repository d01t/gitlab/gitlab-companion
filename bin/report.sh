#!/bin/bash
. "$(dirname "$0")/common.sh"
[ -d build/report ] && rm -Rf build/report
if [ -n "${GITLAB_API_PRIVATE_TOKEN}" ]
then
	# shellcheck disable=SC2089
	CURL="curl --location --silent"
	# shellcheck disable=SC2090 disable=SC2162
	${CURL} --header "Private-Token: ${GITLAB_API_PRIVATE_TOKEN}" "https://build.do-it.guru/api/v4/projects/${CI_PROJECT_ID}/repository/commits?ref_name=${CI_COMMIT_REF_NAME}" | \
		jq '.[].id' | sed -e '1d' -e 's/"//g' | while read PREVIOUS_COMMIT_SHA
	do

		${CURL} --header "Private-Token: ${GITLAB_API_PRIVATE_TOKEN}" "https://build.do-it.guru/api/v4/projects/${CI_PROJECT_ID}/pipelines?sha=${PREVIOUS_COMMIT_SHA}" | \
			jq '.[].id' | while read PREVIOUS_PIPELINE_ID
		do
			PREVIOUS_JOB_ID=$(${CURL} --header "Private-Token: ${GITLAB_API_PRIVATE_TOKEN}" "https://build.do-it.guru/api/v4/projects/${CI_PROJECT_ID}/pipelines/${PREVIOUS_PIPELINE_ID}/jobs" | jq '.[] | select(.stage!="Build") | select(.artifacts[].file_type=="archive") | .id' | tail -n 1)
			if [ -n "${PREVIOUS_JOB_ID}" ]
			then
				${CURL} --header "Private-Token: ${GITLAB_API_PRIVATE_TOKEN}" "https://build.do-it.guru/api/v4/projects/${CI_PROJECT_ID}/jobs/${PREVIOUS_JOB_ID}/artifacts" -o build/artifacts.zip
				unzip -o -q build/artifacts.zip 'build/report/history/*' || true
				rm -f build/artifacts.zip
				if [ -d build/report/history ]
				then
					break
				fi
			fi
		done
		if [ -d build/report/history ]
		then
			break
		fi
	done
fi
mkdir -p build/report/badges build/allure
[ -d build/allure/history ] && rm -Rf build/allure/history
[ -d build/report/history ] && mv build/report/history build/allure/
# shellcheck disable=SC2015
[ -d build/test ] && find build/test -type f -name "*.xml" -exec sed -i -e "s#${DIR}/##g" {} \; || true
if [ -d config/test ] && ls config/test/ | grep -q xsl
then
	for TYPE in config/test/*.xsl
	do
		TYPE=$(basename "${TYPE}" | sed -e 's/\.xsl//g')
		for FILE in build/test/${TYPE}*.xml
		do
			SUFFIX=$(basename "${FILE}" | sed -E "s/^${TYPE}//g" | sed -e 's/\.xml$//g' | sed -E 's/[-_\.]+/ /g')
			xsltproc --stringparam suffix "${SUFFIX}" "config/test/${TYPE}.xsl" "${FILE}" | xmllint --format - > "build/allure/$(uuid)-testsuite.xml"
		done
	done
fi
# shellcheck disable=SC2010 disable=SC2015
[ -d build/test ] && [ -f build/test/xunit.xml ] && rsync -arc build/test/xunit.xml build/allure/ || true
# shellcheck disable=SC2010 disable=SC2015
[ -d build/test ] && ls build/test/ | grep -q cypress && rsync -arc build/test/cypress*.xml build/allure/ || true
if [ -f build/coverage/index.xml ]
then
	COVERAGE=$(grep lines build/coverage/index.xml | sed -n '1p' | sed -e 's/.*percent="//g' -e 's/".*//g')
	echo "Total coverage: ${COVERAGE}%"
fi
allure generate --clean --output build/report build/allure
TOTAL=$(cat build/report/widgets/summary.json | jq '.statistic.total')
PASSED=$(cat build/report/widgets/summary.json | jq '.statistic.passed')
if [ -d build/test ] && ls build/test/ | grep -q cypress
then
	CURRENT_STAGE_TOTAL=0
	CURRENT_STAGE_PASSED=0
	SUITE_UIDS=$(cat build/report/widgets/suites.json | \
		jq '.items[] | select((.name|startswith("Checkstyle")|not) and (.name|startswith("Copy paste")|not) and (.name|startswith("Mess")|not)) | .uid'  | \
		sed -e 's/"//g')
	for SUITE_UID in ${SUITE_UIDS}
	do
		STATISTIC=$(cat build/report/widgets/suites.json | jq '.items[] | select(.uid=="'"${SUITE_UID}"'") | .statistic')
		CURRENT_STAGE_TOTAL=$(expr ${CURRENT_STAGE_TOTAL} + $(echo ${STATISTIC} | jq '.total'))
		CURRENT_STAGE_PASSED=$(expr ${CURRENT_STAGE_PASSED} + $(echo ${STATISTIC} | jq '.passed')) || true
	done
else
	CURRENT_STAGE_TOTAL=${TOTAL}
	CURRENT_STAGE_PASSED=${PASSED}
fi
if [ ${TOTAL} = 0 ]
then
	PERCENT=0
else
	# shellcheck disable=SC2004
	PERCENT=$((100*${PASSED}/${TOTAL}))
fi
if [ ${CURRENT_STAGE_TOTAL} = 0 ]
then
	CURRENT_STAGE_PERCENT=0
else
	# shellcheck disable=SC2004
	CURRENT_STAGE_PERCENT=$((100*${CURRENT_STAGE_PASSED}/${CURRENT_STAGE_TOTAL}))
fi
ALLOW_FAILURE=1
if [ ${PERCENT} -le 30 ]
then
	COLOR="red"
elif [ ${PERCENT} -le 50 ]
then
	COLOR="orange"
elif [ ${PERCENT} -le 70 ]
then
	COLOR="yellow"
else
	COLOR="green"
fi
if [ ${CURRENT_STAGE_PERCENT} -le 30 ]
then
	PREFIX="\033[1;31m💩"
	EXITCODE=3
elif [ ${CURRENT_STAGE_PERCENT} -le 50 ]
then
	PREFIX="\033[1;31m🔥"
	EXITCODE=2
elif [ ${CURRENT_STAGE_PERCENT} -le 70 ]
then
	PREFIX="\033[1;33m😯"
	ALLOW_FAILURE=0
	EXITCODE=1
else
	PREFIX="\033[1;32m👍"
	EXITCODE=0
fi
echo -e "${PREFIX} Test results : ${CURRENT_STAGE_PASSED} passed on ${CURRENT_STAGE_TOTAL}\033[0m"
mkdir -p build/report/badges
curl -s -q -o build/report/badges/report.svg "https://img.shields.io/badge/report-${PERCENT}%25-${COLOR}.svg"
# TODO: store in gitlab pages
exit ${EXITCODE}
