#!/bin/bash
. "$(dirname "$0")/common.sh"
yarn
if [ -n "${JOB}" ]
then
	sed -i -E 's/"version": "([^"\.]+\.[^"\.]+\.[^"\.])[^"]*"/"version": "\1.'"${JOB}"'"/g' package.json
fi
NODE_ENV=production yarn build