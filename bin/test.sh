#!/bin/bash
. "$(dirname "$0")/common.sh"

# Initializing folders
mkdir -p build/logs build/test build/report build/allure

# Lint
yarn lint

# Allow errors
set +e
yarn test

# End without error
exit 0
